#! /bin/sh

echo "\033[1m[\033[34mINFO\033[0m\033[1m]\033[0m"
echo "\033[1m[\033[34mINFO\033[0m\033[1m] 💡 To skip tests on commit, set SKIP_TESTS=true 💡"
echo "\033[1m[\033[34mINFO\033[0m\033[1m]\033[0m"
